package com.example.pruebas

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.pruebas.ui.theme.PruebasTheme
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import kotlin.random.Random


class MainActivity : ComponentActivity() {

        private lateinit var imageView: ImageView
        private lateinit var button: Button
        private lateinit var textView: TextView

        private var currentState = State.EMPTY
        private var fruitCounter = 0

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            val layout = RelativeLayout(this)
            layout.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            imageView = ImageView(this)
            imageView.id = R.id.imageView
            imageView.layoutParams = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            layout.addView(imageView)

            button = Button(this)
            button.id = R.id.button
            button.text = "Pulsar"
            val buttonParams = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            buttonParams.addRule(RelativeLayout.CENTER_HORIZONTAL)
            buttonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
            buttonParams.setMargins(
                0,
                0,
                0,
                resources.getDimensionPixelSize(R.dimen.activity_vertical_margin)
            )
            button.layoutParams = buttonParams
            layout.addView(button)

            textView = TextView(this)
            textView.id = R.id.textView
            val textParams = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            textParams.addRule(RelativeLayout.CENTER_HORIZONTAL)
            textParams.addRule(RelativeLayout.ABOVE, R.id.button)
            textParams.setMargins(
                0,
                0,
                0,
                resources.getDimensionPixelSize(R.dimen.activity_vertical_margin)
            )
            textView.layoutParams = textParams
            layout.addView(textView)

            setContentView(layout)

            button.setOnClickListener {
                when (currentState) {
                    State.EMPTY -> {
                        updateUI(State.PLANTS, R.drawable.huertoplantas)
                    }

                    State.PLANTS -> {
                        updateUI(State.FRUIT, R.drawable.huertofrutos)
                    }

                    State.FRUIT -> {
                        val randomEuros = Random.nextInt(1, 6)
                        textView.text = "Ganaste $randomEuros euros"
                        updateUI(State.EMPTY, R.drawable.huertovacio)
                    }
                }
            }
        }

        private fun updateUI(state: State, imageResource: Int) {
            currentState = state
            imageView.setImageResource(imageResource)
            if (state == State.FRUIT) {
                fruitCounter++
            }
        }

            }



@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    PruebasTheme {
        Greeting("Android")
    }
}

@Composable
fun Greeting(s: String) {
Text(text = "hola")
}
